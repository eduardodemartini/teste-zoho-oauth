import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { HTTP } from '@ionic-native/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private zohoUrl = 'https://accounts.zoho.com/oauth/v2';
  private zohoAuth = this.zohoUrl + '/auth?';
  private zohoToken = this.zohoUrl + '/token?';

  private codeCallback;

  private state = new Date().getTime() + "";

  private browser;

  userName = '';

  userEmail = '';

  constructor(private iab: InAppBrowser, private http: HTTP) {}

  doLogin() {
    const params = [];
    params.push('scope=ZohoMail.accounts.READ');
    params.push('client_id=1000.IM3SZPQ0KLRF09304O0GPFL4CFWBRF');
    params.push('response_type=code');
    params.push('access_type=offline');
    params.push('redirect_uri=http://localhost/callback');
    params.push('state=' + this.state);

    this.browser = this.iab.create(this.zohoAuth + params.join('&'), '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');    

    this.browser.on('loadstart').subscribe(event => {
      if( event.url.indexOf('http://localhost/callback') == 0 ) {
        this.state = event.url.split('?')[1].split('&')[0].substring(6);
        this.codeCallback = event.url.split('?')[1].split('&')[1].substring(5);

        this.getToken();
      }
    });
  }

  private getToken() {
    const params = [];
    params.push('code=' + this.codeCallback);
    params.push('grant_type=authorization_code');
    params.push('client_id=1000.IM3SZPQ0KLRF09304O0GPFL4CFWBRF');
    params.push('client_secret=4e77638567fac5a0931ca612826588400efa14c223');
    params.push('redirect_uri=http://localhost/callback');
    params.push('scope=ZohoMail.accounts.READ');
    params.push('state=' + this.state);

    this.http.post(this.zohoToken + params.join('&'), {}, {}).then(
      data => {
        localStorage.setItem('Zoho-oauthtoken', data.data);

        this.getUserDetails();
      },
      error => console.log('error getToken', error)
    );
  }

  private getUserDetails() {
    this.http.get('https://mail.zoho.com/api/accounts', {}, {
      Authorization: 'Zoho-oauthtoken ' + JSON.parse(localStorage.getItem('Zoho-oauthtoken')).access_token
    }).then(
      data => {
        const jsonData = data.data[0];

        this.userName = jsonData.firstName + ' ' + jsonData.lastName;
        this.userEmail = jsonData.primaryEmailAddress;

        console.log(this.userName, this.userEmail);

        this.browser.close();
      },
      error => console.log('error getUserDetails', JSON.stringify(error))
    );
  }

}
